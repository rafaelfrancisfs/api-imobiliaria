import express from "express";
import {
  promises
} from "fs";

const router = express.Router();

const {
  readFile,
  writeFile
} = promises;

// POST incluir imovel
router.post("/", async (req, res) => {
  let imovel = req.body;
  console.log(imovel);
  try {
    let data = await readFile(global.fileName, "utf8");
    let json = JSON.parse(data);

    imovel = {
      id: json.nextId++,
      ...imovel,
    };
    json.imoveis.push(imovel);

    await writeFile(global.fileName, JSON.stringify(json));
    console.log(json);
    res.send(imovel);
  } catch (error) {
    console.log(error.message);
    res.send("falhou");
  }
});

// GET listagem
router.get("/", async (req, res) => {
  try {
    let data = await readFile(global.fileName, "utf8");
    let json = JSON.parse(data);
    delete json.nextId;
    res.send(json);
  } catch (error) {
    console.log(error.message);
    res.status(400).send({
      erro: error.message
    });
  }
});

// GET filtra por ID
router.get("/:id", async (req, res) => {
  try {
    let data = await readFile(global.fileName, "utf8");
    let json = JSON.parse(data);
    const imovel = json.imoveis.find(
      (imovel) => imovel.id === parseInt(req.params.id, 10)
    );
    if (imovel) {
      res.send(imovel);
    } else {
      res.send("ID invalído");
    }
  } catch (error) {
    console.log(error.message);
    res.status(400).send({
      erro: error.message
    });
  }
});


// DELETE passando ID como parametro

router.delete("/:id", async (req, res) => {
  try {
    let data = await readFile(global.fileName, "utf8");
    let json = JSON.parse(data);
    let imoveis = json.imoveis.filter(
      (imovel) => imovel.id !== parseInt(req.params.id, 10)
    );
    if (imoveis) {
      json.imoveis = imoveis;
      await writeFile(global.fileName, JSON.stringify(json));
      console.log(parseInt(req.params.id, 10))
      console.log(json);
      res.send("delete ok");
    } else {
      res.send("ID invalído");
    }
  } catch (error) {
    console.log(error.message);
    res.status(400).send({
      erro: error.message
    });
  }
});

// PUT atulizar
router.put("/", async (req, res) => {
  let putImovel = req.body;
  try {
    let data = await readFile(global.fileName, "utf8");
    let json = JSON.parse(data);
    let index = json.imoveis.findIndex(imovel => imovel.id === parseInt(putImovel.id));
    console.log("putImovel");
    console.log(putImovel);

    json.imoveis[index].nome = putImovel.nome;
    json.imoveis[index].endereco = putImovel.endereco;
    json.imoveis[index].descricao = putImovel.descricao;
    json.imoveis[index].status = putImovel.status;
    json.imoveis[index].caracteristicas = putImovel.caracteristicas;
    json.imoveis[index].tipo = putImovel.tipo;
    json.imoveis[index].finalidade = putImovel.finalidade;
    json.imoveis[index].nomeImobiliaria = putImovel.nomeImobiliaria;
    json.imoveis[index].enderecoImobiliaria = putImovel.enderecoImobiliaria;
    console.log(index);


    await writeFile(global.fileName, JSON.stringify(json));

    res.send(putImovel);

  } catch (error) {
    console.log(error.message);
    res.status(400).send({
      erro: error.message
    })

  }

});
export default router;