import express from "express";
import {
    promises
} from "fs";
import router from "./routes/router.js";


const app = express();

const {
    readFile,
    writeFile
} = promises;

global.fileName = "imoveis.json";

app.use(express.json());
app.use("/imoveis", router);
app.listen(3000, async () => {
    try {
        await readFile(global.fileName, "utf8");
        console.log("API onLine");
    } catch (err) {
        console.log(err.message);
    }
});